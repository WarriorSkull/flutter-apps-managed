import 'package:flutter/material.dart';

class AnimatedImage extends StatefulWidget {


  @override
  _AnimatedImageState createState() => _AnimatedImageState();
}

class _AnimatedImageState extends State<AnimatedImage>

    with SingleTickerProviderStateMixin {
  late AnimationController _controller = AnimationController(vsync: this,
    duration: const Duration(seconds: 3),

  )..repeat(reverse: true);
  late Animation<Offset> _animation = Tween(
      begin: Offset.zero,
      end: Offset(0, 0.15)
  ).animate(CurvedAnimation(parent: _controller, curve: Curves.elasticInOut));


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SlideTransition(position: _animation,
      child: Image.asset("assets/images/new_logo.png",height: size.height *0.22 ,));
  }
}

