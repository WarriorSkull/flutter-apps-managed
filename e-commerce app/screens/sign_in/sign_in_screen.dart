import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/controllers/authController.dart';
import 'package:ecommerce_app/screens/Login/components/background_login.dart';
import 'package:ecommerce_app/screens/Login/components/login_prompts_user.dart';
import 'package:ecommerce_app/screens/welcome_screen/components/rounded_button.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../constants.dart';
import 'animation_sign_in_screen_image.dart';

class SignInScreen extends GetView<AuthController> {
  final TextEditingController _emailController;
  final TextEditingController _passwordController;
  final TextEditingController _confirmPasswordController;
  final TextEditingController _firstNameController;

  final TextEditingController _lastNameController;

  SignInScreen(
      this._firstNameController,
      this._lastNameController,
      this._confirmPasswordController,
      this._emailController,
      this._passwordController);

  @override
  Widget build(BuildContext context) {
    final topPadding = MediaQuery.of(context).padding.top;
    final _size = MediaQuery.of(context).size;
    return Scaffold(
      // appBar: AppBar(
      //   title: Center(
      //       child: Text(
      //         "Sign Up",
      //         style: TextStyle(color: Colors.black),
      //       )),
      //   backgroundColor: Colors.white,
      //   leading: IconButton(
      //     icon: Icon(
      //       Icons.arrow_back_ios,
      //       color: Colors.black,
      //     ),
      //     onPressed: () {
      //       Get.toNamed(AppRoutes.welcomeRoute);
      //     },
      //   ),
      //   elevation: 0,
      // ),

      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Container(
          color: Colors.black54,
          child: BackgroundLogin(
            child: Center(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: _size.height * 0.08),
                    child: Text(
                      "Register Account",
                      style: TextStyle(
                          fontFamily: 'Circular-Air',
                          fontSize: 30,
                          color: Colors.black),
                    ),
                  ),
                  FittedBox(
                      fit: BoxFit.none,
                      child: Container(
                        child: Text(
                          "Complete your details or continue with social media",
                          style: TextStyle(
                              fontFamily: 'Circular-Air',
                              color: Colors.white,
                              fontSize: 15),
                        ),
                        width: 230,
                      )),
                  SizedBox(
                    height: 150,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 50, right: 14),
                    child: SingleChildScrollView(
                      child: Column(
                      children: [
                        SignUpWidget(
                          icon: CupertinoIcons.person_alt,
                          hintText: "Enter Your First Name",
                          obscureText: false,
                          textEditingController: _firstNameController,
                        ),
                        // SignUpWidget(
                        //   icon: CupertinoIcons.person,
                        //   hintText: "Enter Your Last Name",
                        //   obscureText: false,
                        //   textEditingController: _lastNameController,
                        // ),
                        SignUpWidget(
                          icon: CupertinoIcons.envelope,
                          hintText: "Enter Your Email Address",
                          obscureText: false,
                          textEditingController: _emailController,
                        ),
                        SignUpWidget(
                          icon: CupertinoIcons.lock,
                          hintText: "Enter Your Password",
                          obscureText: true,
                          textEditingController: _passwordController,
                        ),
                        SignUpWidget(
                          icon: CupertinoIcons.lock_shield,
                          hintText: "Confirm your password",
                          obscureText: true,
                          textEditingController: _confirmPasswordController,
                        ),
                      ],
                    ),
                    ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SignUpWidget extends StatelessWidget {
  final String hintText;
  final IconData icon;
  static String email = "";
  final bool obscureText;
  final TextEditingController textEditingController;

  const SignUpWidget({
    Key? key,
    required this.hintText,
    required this.icon,
    required this.obscureText,
    required this.textEditingController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: Form(
        child: TextField(
            cursorColor: Colors.green,
            controller: textEditingController,
            obscureText: obscureText,
            decoration: InputDecoration(
              hintText: hintText,
              icon: Icon(
                icon,
                color: kPrimaryColor,
              ),
              border: InputBorder.none,
            )),
      ),
    );
  }
}
