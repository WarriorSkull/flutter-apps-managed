import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:ecommerce_app/controllers/shopping_controller.dart';
import 'package:ecommerce_app/models/Product.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartScreen extends StatefulWidget {


  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {


  final cartController = Get.put(CartController());





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.only(left: 75),
          child: Column(
            children: [
              Text(

                "Your cart",
                style: TextStyle(
                    fontFamily: 'Circular-Air',
                    fontWeight: FontWeight.w500,
                    fontSize: 25,
                    color: Colors.black),
              ),
              Text(
                "4 Items",
                style: TextStyle(
                    fontFamily: 'Circular-Air',
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: Colors.black),
              )
            ],
          ),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Get.offAllNamed(AppRoutes.homeRoute);
          },
        ),
      ),
      body: Row(
        children: [
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(color: Color(0xffbbaef1), borderRadius: BorderRadius.circular(15),),
                // child: Image.asset(),

              ),
            ),
          ),
        ],
      ),
      // body: Container(child: Obx(() {
      //   return Container(
      //     padding: EdgeInsets.only(left: 4),
      //     color: Colors.white,
      //     child: Center(
      //       child: Text(
      //         "Total Amount: \$ ${_cartController.totalPrice.value}",
      //         style: TextStyle(
      //           fontWeight: FontWeight.bold,
      //           fontSize: 25,
      //           color: Colors.black,
      //           backgroundColor: Colors.white,
      //         ),
      //       ),
      //     ),
      //   );
      // })),
    );
  }
}
