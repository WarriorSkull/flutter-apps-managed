import 'dart:ui';

import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:ecommerce_app/models/Product.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'add_to_cart.dart';
import 'color_and_size.dart';
import 'counter_With_fav_btn.dart';
import 'product_description.dart';
import 'product_title_With_image.dart';

class InsideBody extends StatelessWidget {
  final Product product;


  const InsideBody({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // it provide us total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: size.height * 0.5),
              padding: EdgeInsets.only(
                  top: size.height * 0.10,
                  left: kDefaultPaddin,
                  right: kDefaultPaddin),

              //height: 500,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24))),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  ColorAndSize(product: product),
                  SizedBox(height: kDefaultPaddin/2,),
                  Description(product: product),
                  SizedBox(height: kDefaultPaddin/2,),
                  CounterWithFaveBtn(),
// open cart counter for adding or removing products
                  AddToCart(product: product)
                ],
              ),
            ),
            ProductTiltewithImage(product: product)
          ],
        ),
      ),
    );
  }
}

