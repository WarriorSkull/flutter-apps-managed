import 'package:ecommerce_app/models/Product.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class ProductTiltewithImage extends StatelessWidget {
  const ProductTiltewithImage({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            ("J. Kurtas"),
            style: TextStyle(color: Colors.white),
          ),
          Text(
            product.title,
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 30,
          ),
          Row(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,

              children: <Widget>[
                RichText(
                    text: TextSpan(children: [
                  TextSpan(
                      text: "Price\n",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(
                      text: "\$${product.price}",
                      style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold))
                ])),
                SizedBox(
                  width: kDefaultPaddin,
                ),
                Flexible(fit: FlexFit.tight ,
                  child: Container(
                    width: MediaQuery.of(context).size.width / 0.7,
                      height: 290,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: product.images.length ,
                        separatorBuilder:(BuildContext, context) => SizedBox(width: 6,),
                        itemBuilder:(BuildContext, index) => ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                            child: Hero(
                              tag: "${product.id}$index",
                              child: Image.asset(
                                product.images[index],
                                width: 220,
                                height: 290,
                                // width: MediaQuery.of(context).size.width / 4,
                                alignment: Alignment(2, 7),
                             fit: BoxFit.cover, ),
                            )
                        ),
                      )),
                )
              ],
            )
        ],
      ),
    );
  }
}
