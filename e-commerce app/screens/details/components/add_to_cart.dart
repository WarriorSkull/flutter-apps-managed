import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:ecommerce_app/models/Product.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../constants.dart';

class AddToCart extends StatelessWidget {
  final cartController = Get.put(CartController());
   AddToCart({
    Key? key,
    required this.product,
  }) : super(key: key);

final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: kDefaultPaddin),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: kDefaultPaddin),
            height: 50,
            width: 58,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: product.color),
            ),
            child: IconButton(
              icon: SvgPicture.asset(
                "assets/icons/add_to_cart.svg", color:product.color ,),
              onPressed: () {},
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 50,

              child: TextButton(
                style: TextButton.styleFrom(
                    backgroundColor: product.color
                ),
                onPressed: () {
                  print("i am working");
                  cartController.calculateTotalPrice();
                  Get.toNamed(AppRoutes.cartPage);

                  },
                child: Text("Buy Now".toUpperCase(),
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,


                  ),),
              ),
            ),
          )
        ],
      ),
    );
  }
}
