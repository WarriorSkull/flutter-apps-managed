import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants.dart';

class CartCounter extends StatelessWidget{

  final CartController _cartController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[

        buildOutlineButton(
          icon: Icons.remove,
          press: () {
            if(_cartController.cardCount.value == 1){
              return;
            }else{
              _cartController.removeCount();
            }
          },
        ),
        Obx((){
          return  Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin / 2),
            child: Text(
              // if our item is less  then 10 then  it shows 01 02 like that
              _cartController.cardCount.value.toString().padLeft(2, "0"),
              style: Theme
                  .of(context)
                  .textTheme
                  .headline6,
            ),
          );
        }),
        buildOutlineButton(
            icon: Icons.add,
            press: () {
              if(_cartController.cardCount.value == 10){
                return;
              }else{
                _cartController.addCount();
              }
            }),
      ],
    );
  }

  SizedBox buildOutlineButton(
      {required IconData icon, required VoidCallback press}) {
    return SizedBox(
      width: 40,
      height: 32,
      child: MaterialButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        onPressed: press,
        child: Icon(icon),
      ),
    );
  }
}