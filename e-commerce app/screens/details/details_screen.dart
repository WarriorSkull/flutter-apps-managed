import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:ecommerce_app/models/Product.dart';
import 'package:ecommerce_app/screens/details/components/body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../constants.dart';

class DetailsScreen extends StatelessWidget {
  final Product product;
  final CartController _cartController = Get.put(CartController());

  DetailsScreen({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _cartController.product.value = product;
    return Scaffold(
      // each product have a color
        backgroundColor: product.color,
        appBar: buildAppBar(context),
        body: Padding(
            padding: EdgeInsets.all(5),
            child: InsideBody(product: product,)
        ),
    );
  }
      AppBar buildAppBar(BuildContext context) {
      return AppBar(
          backgroundColor: product.color,
          elevation: 5,
          actions: <Widget>[
            // IconButton(
            //   onPressed: () {},
            //   icon: SvgPicture.asset("assets/icons/search.svg"),
            // ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset("assets/icons/cart.svg"),
              color: kTextColor,
            ),
            SizedBox(
              width: kDefaultPaddin / 2,
            ),
          ]
      );

  }
}
