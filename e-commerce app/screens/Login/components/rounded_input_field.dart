

import 'package:flutter/material.dart';

import '../../../constants.dart';
import 'login_prompts_user.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final Icon suffixIcon;
  final ValueChanged<String> onChanged;
  final TextEditingController editingController;
  final bool isPasswordField;
  const RoundedInputField({
    Key? key,
    required this.hintText,
    this.icon = Icons.person,
    required this.onChanged,
    required this.suffixIcon ,
    required this.editingController,
    this.isPasswordField = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TextFieldContainer saved in file name login_prompt_user.
    return TextFieldContainer(
      child: TextField(

        controller: editingController,
          obscureText: isPasswordField,
          decoration: InputDecoration(
              hintText: hintText,
              icon: Icon(
                icon,
                color: kPrimaryColor,
              ),
              suffixIcon: suffixIcon,
              border: InputBorder.none)),
    );
  }
}

