import 'dart:math';

import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/controllers/authController.dart';
import 'package:ecommerce_app/controllers/bindings/authBinding.dart';
import 'package:ecommerce_app/controllers/login_controller.dart';
import 'package:ecommerce_app/models/user_model.dart';
import 'package:ecommerce_app/screens/sign_in/animation_sign_in_screen_image.dart';
import 'package:ecommerce_app/screens/sign_in/sign_in_screen.dart';
import 'package:ecommerce_app/screens/welcome_screen/components/rounded_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'background_login.dart';
import 'login_prompts_user.dart';
import 'rounded_input_field.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> with SingleTickerProviderStateMixin {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
  TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final AuthController _authController = Get.find();

  late AnimationController _animationController;
  bool _isShowSignUp = false;
  late Animation<double> _animationTextRotate;

  void _setUpAnimation() {
    _animationController =
        AnimationController(vsync: this, duration: defaultDuration);
    _animationTextRotate =
        Tween<double>(begin: 0, end: 90).animate(_animationController);
  }

  void updateView() {
    setState(() {
      _isShowSignUp = !_isShowSignUp;
    });
    _isShowSignUp
        ? _animationController.forward()
        : _animationController.reverse();
  }

  @override
  void initState() {
    // TODO: implement initState
    _setUpAnimation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return BackgroundLogin(
    //     child: Column(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: <Widget>[
    //           Text(
    //             "LOGIN",
    //             style: TextStyle(fontWeight: FontWeight.bold),
    //           ),
    //           AnimatedImage(),
    //           Container(
    //             child: Form(
    //               key: _formKey,
    //               child: Column(
    //                 children: [
    //                   Container(
    //                     child: TextFormField(
    //                       validator: (input){
    //                         if(input!.isEmpty)
    //                           return 'Enter Email';
    //
    //                     },decoration: InputDecoration(
    //                       labelText: 'Email',prefixIcon: Icon(Icons.email_outlined)
    //                     ),
    //                       onSaved: (input) => _email = input,
    //
    //                     ),
    //                   ),
    //                   Container(
    //                     child: TextFormField(
    //                       validator: (input){
    //                         if(input!.length <6)
    //                           return 'Provide Minimum 6 character';
    //
    //                       },decoration: InputDecoration(
    //                         labelText: 'Password',prefixIcon: Icon(Icons.lock_outline_rounded),
    //
    //                     ),
    //                       obscureText: true,
    //                       onSaved: (input) => _password = input,
    //
    //                     ),
    //                   ),
    //
    //                 ],
    //               ),
    //             ),
    //
    //           )
    //         ])
    // );
    final _size = MediaQuery
        .of(context)
        .size;
    return AnimatedBuilder(
        animation: _animationController,
        builder: (context, _) {
          return Stack(fit: StackFit.loose, children: [
            //Login
            AnimatedPositioned(
              duration: defaultDuration,
              width: _size.width * 0.88,
              // 88%
              height: _size.height,
              left: _isShowSignUp ? -_size.width * 0.76 : 0,
              // 76%
              child: LoginField(_emailController, _passwordController),
            ),

            // SignUp
            AnimatedPositioned(
              duration: defaultDuration,
              width: _size.width * 0.88,
              height: _size.height,
              left: _isShowSignUp ? _size.width * 0.12 : _size.width * 0.88,
              child: SignInScreen(
                  _firstNameController,
                  _lastNameController,
                  _confirmPasswordController,
                  _emailController,
                  _passwordController),
            ),
            AnimatedPositioned(
              duration: defaultDuration,
              top: _isShowSignUp ? _size.height * 0.14 : _size.height * 0.14,
              left: _isShowSignUp ? _size.width * 0.01 : -_size.width * 0.15,
              right: -_size.width * 0.06,
              child: AnimatedSwitcher(
                  duration: defaultDuration, child: AnimatedImage()),
            ),

            // login Text animated
            AnimatedPositioned(

                duration: defaultDuration,
                bottom: _isShowSignUp
                    ? _size.height / 2 - 150
                    : _size.height * 0.28,
                left: _isShowSignUp ? 0 : _size.width * 0.44 - 80,
                child: AnimatedDefaultTextStyle(
                  duration: defaultDuration,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: _isShowSignUp ? 22 : 37,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Circular-Air',
                      color: _isShowSignUp
                          ? CupertinoColors.white
                          : Colors.white70),
                  child: Transform.rotate(
                    angle: -_animationTextRotate.value * pi / 180,
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      onTap: () {
                        if (_isShowSignUp) {
                          updateView();
                        } else {
                          // login
                          _authController.login(
                              _emailController.text, _passwordController.text);
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 14),
                        // color: Colors.red,
                        width: 160,
                        child: Text(
                          "Log In".toUpperCase(),
                        ),
                      ),
                    ),
                  ),
                )),
            // animated signup Text
            AnimatedPositioned(
                duration: defaultDuration,
                bottom: !_isShowSignUp
                    ? _size.height / 2 - 150
                    : _size.height * 0.09,
                right: _isShowSignUp ? _size.width * 0.44 - 90 : 0,
                child: AnimatedDefaultTextStyle(
                  duration: defaultDuration,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: !_isShowSignUp ? 22 : 32,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Circular-Air',
                      color: _isShowSignUp
                          ? Colors.white70
                          : CupertinoColors.white),
                  child: Transform.rotate(
                    angle: (90 - _animationTextRotate.value) * pi / 180,
                    alignment: Alignment.topRight,
                    child: InkWell(
                      onTap: () {
                        if (_isShowSignUp) {
                          //signup
                          _authController.createUser(
                              _firstNameController.text,
                              _lastNameController.text,
                              _emailController.text,
                              _passwordController.text,
                              _confirmPasswordController.text);
                        } else {
                          updateView();
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 14),
                        // color: Colors.red,
                        width: 160,
                        child: Text(
                          "Sign up".toUpperCase(),
                        ),
                      ),
                    ),
                  ),
                ))
          ]);
        });
  }
}

class LoginField extends GetView<AuthController> {
  final TextEditingController emailController;

  final TextEditingController passwordController;

  final AuthController _authController = Get.find();

  LoginField(this.emailController, this.passwordController);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery
        .of(context)
        .size;
    return Container(
      color: Color(0xF744EE86),
      child: BackgroundLogin(
        child: Padding(
          padding: EdgeInsets.only(top: size.height * 0.1),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  bottom: 180,
                ),
                child: Text(
                  "LOGIN",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(
                    right: size.width * 0.119, left: size.width * 0.05),
                child: Column(
                  children: [
                    RoundedInputField(
                      onChanged: (String value) {},
                      hintText: "Your Email",
                      suffixIcon: Icon(null),
                      editingController: emailController,
                    ),
                    TextFieldContainer(
                      child: TextField(
                          controller: passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                            hintText: "Password",
                            icon: Icon(
                              Icons.lock,
                              color: kPrimaryColor,
                            ),
                          )),
                    ),
                  ],
                ),
              ),

              // Obx((){
              //   return TextFieldContainer(
              //     child: TextField(
              //         controller: loginController.passwordController,
              //         obscureText: loginController.isPasswordVisible.value,
              //         decoration: InputDecoration(
              //             hintText: "Password",
              //             icon: Icon(
              //               Icons.lock,
              //               color: kPrimaryColor,
              //             ),
              //             suffixIcon: GestureDetector(
              //               onTap: (){
              //                 loginController.isPasswordVisible.value = !loginController.isPasswordVisible.value;
              //               },
              //               child: loginController.isPasswordVisible.value ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
              //             ),
              //             border: InputBorder.none)),
              //   );
              // }),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(
                      fontFamily: 'Circular',
                      fontStyle: FontStyle.italic,
                      fontSize: 17,
                      decoration: TextDecoration.underline),
                ),
                onTap: () {
                  Get.toNamed(AppRoutes.passwordResetRoute);
                },
              ),
              // RoundedButton(
              //   press: () {
              //     _authController.login(
              //         _emailController.text, _passwordController.text);
              //   },
              //   text: "LOGIN",
              //   textColor: Colors.white,
              //   buttonColor: CupertinoColors.systemPurple,
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

class PasswordResetVerification extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final AuthController _authController = Get.find();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery
        .of(context)
        .size;
    var page = Scaffold(appBar:
    AppBar(
      backgroundColor: Colors.transparent,
      title: Text("Password Reset ",
        style: TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
          fontFamily: 'Circular-Air',
          color: Colors.black26,
        )),),
      body: Container(
        width: size.width,
        color: Color(0xF744EE86),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      "Please type in your registered email address and check your email for the password reset link",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Circular-Air',
                        color: CupertinoColors.white,
                      ), softWrap: true,),
                  ),
                ],
              ),
            ),
            SizedBox(height: 70,),
            SignUpWidget(
                hintText: "Your Email",
                icon: CupertinoIcons.mail_solid,
                obscureText: false,
                textEditingController: _emailController),
            // RoundedButton(
            //     text: "Send Me Email",
            //     press: () =>
            //         _authController.resetPassword(_emailController.text),
            //     buttonColor: Colors.purple),
            Container(
              child: InkWell(
                child: Text("Send me Email!",
                    style: TextStyle(
                      fontSize: 37,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Circular-Air',
                      color: Colors.white70,
                    )),
                onTap: () =>
                    _authController.resetPassword(_emailController.text),
              ),
            )
          ],
        ),
      ),
    );
    return page;
  }
}
