import 'package:ecommerce_app/controllers/cart_controller.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentPrice extends StatefulWidget {
  @override
  _PaymentPriceState createState() => _PaymentPriceState();
}

class _PaymentPriceState extends State<PaymentPrice> {
  final cartController = Get.put(CartController());

  final CartController _cartController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(child: Obx(() {
        return Container(

          padding: EdgeInsets.only(left: 4),
          color: Colors.white,
          child: Center(
            child: Text(

              "Total Amount: \$ ${_cartController.totalPrice.value}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.black,
                backgroundColor: Colors.white,

              ),

            ),
          ),
        );
      })),
    );
  }
}
