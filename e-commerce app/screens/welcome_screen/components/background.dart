import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;

  const Background({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            color: Colors.white,
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     image: AssetImage('assets/images/Welcome_background.jpg'),
            //     fit: BoxFit.fill,
            //   ),
            // ),
          ),
          Positioned(
              top: 0,
              left: 0,
              child: Image.asset(
                "assets/images/main_top.png",
                width: size.width * 0.3,
              )),
          Positioned(
            top: 0,
            left: size.width / 0.9,
            child: Transform(
                transform: new Matrix4.identity()
                  ..rotateZ(90 * 3.1415927 / 180),
                child: Image.asset(
                  "assets/images/main_top.png",
                  width: size.width * 0.5,
                  color: Colors.blueAccent,
                )),
          ),
          Positioned(
              bottom: -8,
              left: -8,
              child: Image.asset(
                "assets/images/main_bottom.png",
                width: size.width * 0.4,
              )),
          child,
        ],
      ),
    );
  }
}
