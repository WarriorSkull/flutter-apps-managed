import 'package:flutter/material.dart';

import '../../../constants.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final void Function() press;
  final Color buttonColor, textColor;

  const RoundedButton({
    Key? key,
    required this.text,
    required this.press,

    this.textColor= Colors.white, required this.buttonColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
        width: size.width * 0.45,
        height: size.height *0.09,
        child: ClipRect(
            child: TextButton(
              onPressed: press,
              child: Text (text,style: TextStyle(fontSize: 24),),
              style: ButtonStyle(
                  side: MaterialStateProperty.all(
                      BorderSide(width: 1, color: Colors.white)),
                  foregroundColor: MaterialStateProperty.all<Color>(textColor),
                  backgroundColor: MaterialStateProperty.all(buttonColor),
                  padding: MaterialStateProperty.all(
                      EdgeInsets.symmetric(vertical: 20, horizontal: 40)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      )
                    // primary: Colors.white,TextButton.styleFrom(
                    // padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                    // backgroundColor: kPrimaryColor,
                    // primary: Colors.white,

                  )),
            )));
  }
}
