import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'background.dart';
import 'rounded_button.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(

        child: Column(

            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text("WELCOME TO J.F MAKERS",
                  textAlign: TextAlign.center,
                  style: TextStyle(

                      fontWeight: FontWeight.bold,
                      fontSize: 50,
                      fontFamily: 'CircularAir'),
                ),

              SizedBox(
                height: size.height * 0.05,
              ),
              SizedBox(
                height: size.height * 0.03,
              ),
              RoundedButton(
                text: "LOGIN",
                buttonColor: Colors.black54,

                press: ()=> Get.toNamed(AppRoutes.loginRoute)
              ),
              RoundedButton(
                buttonColor: Color(0xFF6F35A5),
                text: "Sign up",
                textColor: Colors.black,

                press: () {

                }
                    // Get.toNamed(AppRoutes.signupRoute),
              )
            ]),
      ),
    );
  }
}
