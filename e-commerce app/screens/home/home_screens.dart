import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/components/body.dart';
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/controllers/authController.dart';
import 'package:ecommerce_app/controllers/cart_controller.dart';
import 'package:ecommerce_app/controllers/login_controller.dart';
import 'package:ecommerce_app/controllers/shopping_controller.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  LoginController loginController = Get.find();
  AuthController _authController = Get.find();
  final _cartController = Get.put(CartController());

  // final shoppingController = Get.put(ShoppingController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      // drawer: myDrawer(loginController: loginController),
      body: Body(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xB0FADBDB),
      //Colors.yellow
      elevation: 0,
      title: Text(
        'J.F Makers',
        style: TextStyle(
            color: Colors.black,
            fontStyle: FontStyle.italic,
            wordSpacing: 5,
            fontSize: 30),
      ),

      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(
            CupertinoIcons.search,
            color: kTextColor,
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(CupertinoIcons.cart),
          color: kTextColor,
        ),
        IconButton(
          onPressed: () {
            //loginController.deleteUserSession();
            Get.defaultDialog(
              title: "Logout",
              content: Text("Are you sure you want to logout?"),
              barrierDismissible: false,
              backgroundColor: Colors.red,
              confirmTextColor: Colors.white,
              confirm: MaterialButton(
                onPressed: () {
                  loginController.deleteUserSession();
                  Get.offAllNamed(AppRoutes.welcomeRoute);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(22.0)),
                elevation: 4,
                animationDuration: Duration(milliseconds: 600),
                color: Colors.purple,
                child: Text(
                  "Ok",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              cancel: MaterialButton(
                onPressed: () {
                  Get.back();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(22.0)),
                elevation: 4,
                animationDuration: Duration(milliseconds: 600),
                color: Colors.purple,
                child: Text(
                  "Cancel",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            );
          },
          icon: Icon(CupertinoIcons.clear_fill),
          color: kTextColor,
        ),
        SizedBox(
          width: kDefaultPaddin / 2,
        ),
      ],
    );
  }
}

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  CustomDrawerState createState() => CustomDrawerState();
}

class CustomDrawerState extends State<CustomDrawer>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  // LoginController loginController = Get.find();
  AuthController authController = Get.put(AuthController());
  final _cartController = Get.put(CartController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 250),

    );
  }

  void toggle() => animationController.isDismissed
      ? animationController.forward()
      : animationController.reverse();

  @override
  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Color(0xB0FADBDB),
      //Colors.yellow
      elevation: 0,
      title: Text(
        'J.F Makers',
        style: TextStyle(
            color: Colors.black,
            wordSpacing: 4,
            fontSize: 25,fontFamily: 'CircularAir'),
      ),

      actions: <Widget>[

        IconButton(
          onPressed: () {},
          icon: Icon(
            CupertinoIcons.search,
            color: kTextColor,
          ),
        ),
        IconButton(
          onPressed: ()=> Get.offAllNamed(AppRoutes.cartPage),
          icon: Icon(CupertinoIcons.cart),
          color: kTextColor,
        ),
        IconButton(
          onPressed: () {
            //loginController.deleteUserSession();
            Get.defaultDialog(
              title: "Logout",
              content: Text("Are you sure you want to logout?"),
              barrierDismissible: false,
              backgroundColor: Colors.red,
              confirmTextColor: Colors.white,
              confirm: MaterialButton(
                onPressed: () {
                  // loginController.deleteUserSession();
                  authController.signOut();
                  Get.offAllNamed(AppRoutes.welcomeRoute);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(22.0)),
                elevation: 4,
                animationDuration: Duration(milliseconds: 600),
                color: Colors.purple,
                child: Text(
                  "Ok",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              cancel: MaterialButton(
                onPressed: () {
                  Get.back();
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(22.0)),
                elevation: 4,
                animationDuration: Duration(milliseconds: 600),
                color: Colors.purple,
                child: Text(
                  "Cancel",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            );
          },
          icon: Icon(CupertinoIcons.clear_fill),
          color: kTextColor,
        ),
        SizedBox(
          width: kDefaultPaddin / 2,
        ),
      ],
      leading: IconButton(
        icon: Icon(Icons.menu_rounded),
        onPressed: toggle,
      ),
    );
  }

  final double maxSlide = 225.0;

  @override
  Widget build(BuildContext context) {
    // Drawer(
    //   elevation: 5,
    //   child: ListView(
    //     shrinkWrap: true,
    //     children: [
    //       DrawerHeader(
    //         decoration: BoxDecoration(
    //           color: Colors.green,
    //         ),
    //         duration: Duration(milliseconds: 700) ,
    //         child: Text(
    //           'Drawer Header',
    //           style: TextStyle(
    //             color: Colors.white,
    //             fontSize: 24,
    //           ),
    //         ),
    //       ),
    //       Obx(() {
    //         return loginController.isSessionExist.value
    //             ? Center(
    //             child: Text(
    //               "Welcome, ${loginController.userModel.value.username}",
    //               style: TextStyle(
    //                   color: kTextColor,
    //                   fontWeight: FontWeight.bold,
    //                   fontSize: 20, backgroundColor: Colors.black26),
    //             ))
    //             : Container();
    //       }),
    //       ListTile(
    //         onTap: () {
    //           //goto setting page
    //         },
    //         title: Text("Settings"),
    //         leading: Icon(CupertinoIcons.settings),
    //
    //       ),
    //
    //     ],
    //   ),
    // );
    var myChild = Scaffold(
      appBar: buildAppBar(context),
      // drawer: myDrawer(loginController: loginController),
      body: Body(),
    );

    return GestureDetector(
      onTap: toggle,
      child: AnimatedBuilder(
          animation: animationController,
          builder: (context, _) {
            double slide = maxSlide * animationController.value;
            double scale = 1 - (animationController.value * 0.4);
            return Stack(
              children: [
                DrawerType(),
                Transform(
                    transform: Matrix4.identity()
                      ..translate(slide)
                      ..scale(scale),
                    alignment: Alignment.centerLeft,
                    child: myChild,)
              ],
            );
          }),
    );
  }
}

class DrawerType extends StatelessWidget {
  final AuthController authController = Get.find();
   DrawerType({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
//color: Colors.greenAccent,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [primaryGreen, Color(0xff83efa0)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 80, bottom: 20),
              child: Text(
                "J.F MAKERS".toUpperCase(),
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 45,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Divider(thickness: 2,color: Colors.black,),
            Text("${authController.firstNameUser.toString()}", style: TextStyle( fontSize: 17 ),)



          ],
        ),
      ),
    );
  }
}

