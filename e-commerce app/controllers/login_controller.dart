
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/models/user_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController extends GetxController{

  var userModel = UserModel(username: "", password: "").obs;
  var usernameController = TextEditingController();
  var passwordController = TextEditingController();
  var isSessionExist = false.obs;
  var isPasswordVisible = true.obs;

  @override
  void onInit() {
    getUserSession();

    super.onInit();
  }

  saveUserSession(userObj) async{
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(LOGIN_USER_KEY, userToJson(userObj));
    getUserSession();
  }

  deleteUserSession() async{
    var prefs = await SharedPreferences.getInstance();
    prefs.remove(LOGIN_USER_KEY);
    isSessionExist .value = false;
  }

  getUserSession() async{
    var prefs = await SharedPreferences.getInstance();
    String? data = prefs.getString(LOGIN_USER_KEY);
    if(data != null){
      print("data => $data");
      userModel.value = userFromJson(data);
      isSessionExist.value = true;
    }else{
      isSessionExist.value = false;
    }
  }

}