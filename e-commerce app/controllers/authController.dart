import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  FirebaseAuth _auth = FirebaseAuth.instance;
  Rxn<User> _firebaseUser = Rxn<User>();

  var firstNameUser;
  var lastNameUser;

  String? get user => _firebaseUser.value!.email;

  @override
  void onInit() {
    _firebaseUser.bindStream(_auth.authStateChanges());
  }

  void deleteUser() {
    // make phone verification
  }
void resetPassword(String email)async {
    try{
      await _auth.sendPasswordResetEmail(email: email);

    }
    catch(e){
      SnackBar(Exception,"Please try again");
    }
}
  void createUser(
      var firstName, var lastName, String email, String password, String confirmPassword) async {
    try {
      if(password != confirmPassword){
        throw Exception("Password is not same , Please try again");
      }else {
        await _auth.createUserWithEmailAndPassword(
            email: email, password: password);
        firstNameUser = firstName;
        lastNameUser = lastName;
        Get.toNamed(AppRoutes.loginRoute);
      } } catch (Exception) {
      SnackBar(Exception.toString(),"Error creating your account");

    }
  }

  Future<void> SnackBar(Object Exception, String text) async {
     Get.snackbar(text, Exception.toString(),
        snackPosition: SnackPosition.BOTTOM,
        borderRadius: 17.0,backgroundColor: Colors.yellow,messageText: Text(Exception.toString(),),
        backgroundGradient: LinearGradient(
            colors: [Colors.blue, Colors.green],
            begin: Alignment.topCenter,
            end: Alignment.bottomLeft),animationDuration: Duration(milliseconds: 150),);

  }

  void login(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      Get.toNamed(AppRoutes.homeRoute);
    } catch (e) {
     SnackBar(Exception, "Error logging into your account");
    }
  }

  void signOut() async {
    try {
      await _auth.signOut();
    } catch (e) {
     SnackBar(Exception, "Error Signing out ");
    }
  }
}
