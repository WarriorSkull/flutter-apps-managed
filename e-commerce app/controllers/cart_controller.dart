import 'dart:ui';

import 'package:ecommerce_app/models/Product.dart';
import 'package:get/get.dart';

class CartController extends GetxController {
  var cardCount = 1.obs;
  var product = Product(
          description: '',
          color: Color(0xFF000000),
          title: '',
          id: 0,
          image: "",
          images: [],
          price: 0,
          size: 0)
      .obs;

  //double get totalPrice => cartItems.fold(0, (sum, item) => sum+ item.price);
  var totalPrice = 0.obs;

  @override
  void onInit() {
    super.onInit();

    if (Get.arguments != null) product.value = Get.arguments;
    print("${product.value.price}");
  }

  addToCart(Product product) {
    //cartItems.add(product);
  }

  void addCount() {
    cardCount.value++;
    print("${cardCount.value}");
  }

  void calculateTotalPrice() {
    print('price ${product.value.price.toString()}');
    totalPrice.value = product.value.price * cardCount.value;
    print('${totalPrice.value.toString()}');
  }

  void removeCount() {
    if (cardCount > 0) {
      cardCount.value--;
    }
  }
}
