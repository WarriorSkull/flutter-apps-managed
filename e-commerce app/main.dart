import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/controllers/bindings/authBinding.dart';
import 'package:ecommerce_app/controllers/login_controller.dart';
import 'package:ecommerce_app/screens/sign_in/sign_in_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/authController.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    // MaterialApp(home:SignInScreen() ,)
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  final loginController = Get.put(LoginController());
  final AuthController _authController = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      color: Colors.black,
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        primarySwatch: Colors.blue,
      ),
      initialBinding: AuthBinding(),
      getPages: AppRoutes.routes ,
      initialRoute: loginController.isSessionExist.value
          ? AppRoutes.homeRoute
          : AppRoutes.welcomeRoute,

      //home: //SignInScreen()
      //HomeScreen()
      //WelcomeScreen()
    );
  }
}
