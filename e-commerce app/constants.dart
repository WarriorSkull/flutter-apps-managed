import 'package:flutter/material.dart';

const kTextColor = Color(0xFF2D2C2C);
const kTextLightColor = Color(0xFF494747);
const primaryGreen = Color(0xff416d6d);
const kDefaultPaddin = 20.0;

const kPrimaryColor = Color(0xFF6F35A5);
const kPrimaryLightColor = Color(0xFFF1E6FF);

const LOGIN_USER_KEY = "login_user";
const defaultDuration = Duration(milliseconds: 300);