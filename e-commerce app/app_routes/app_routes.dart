import 'package:ecommerce_app/screens/Cart/cart_screen.dart';
import 'package:ecommerce_app/screens/Login/components/body.dart';
import 'package:ecommerce_app/screens/Login/login_screen.dart';
import 'package:ecommerce_app/screens/details/details_screen.dart';
import 'package:ecommerce_app/screens/sign_in/sign_in_screen.dart';
import 'package:ecommerce_app/screens/welcome_screen/Welcome_screen.dart';
import 'package:get/get.dart';
import 'package:ecommerce_app/screens/home/home_screens.dart';

class AppRoutes{

  static String homeRoute = '/Home';
  static String welcomeRoute = '/Welcome';
  static String loginRoute = '/Login';
  static String  passwordResetRoute= '/passwordReset';
  static String homeDetailsRoute = '/homeDetails';
  static String cartPage = '/cart_screen';
  // static String signupRoute = '/sign_in';


  static final routes = [
    // GetPage(name: homeRoute, page: () => HomeScreen()),
    GetPage(name: homeRoute, page: () => CustomDrawer()),
    GetPage(name: passwordResetRoute, page: () => PasswordResetVerification()),
    GetPage(name: welcomeRoute, page: () => WelcomeScreen()),
    GetPage(name: loginRoute, page: () => LoginScreen()),
    // GetPage(name: signupRoute, page: () => SignInScreen()),
    GetPage(name: homeDetailsRoute, page: () => DetailsScreen(product: Get.arguments,)),
    GetPage(name: cartPage, page: ()=> CartScreen()),
  ];

}