import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int price, size, id;
  final Color color;
   List<String> images;
  static const value= [];
  Product({

    this.id = 0,
    this.image = "",
    this.title = "",
    this.price = 0,
    this.description = "",
    this.size = 0,
    this.color = Colors.black,
    required this.images,
});

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["id"],
    image: json["image"],
    title: json["title"],
    price: json["price"],
    description: json["description"],
    size: json["size"],
    color: json["color"],
    images: json["images"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "image": image,
    "title": title,
    "price": price,
    "description": description,
    "size": size,
    "color": color,
  };

}

List<Product> products = [
  Product(
      id: 1,
      title: "Kurta_Men",
      price: 234,
      size: 10,
      images: ["assets/images/man1.png","assets/images/man_1_1.png","assets/images/man_1_2.png"],
      description: dummyText,
      image: "assets/images/man1.png",
      color: Color(0xFF84B4B4)),
  Product(
      id: 2,
      title: "Kurta_Blue",
      price: 234,
      size: 8,
      images: ["assets/images/man2.png","assets/images/man_2_1.png","assets/images/man_2_2.png"],
      description: dummyText,
      image: "assets/images/man2.png",
      color: Color(0xFFD3A984)),
  Product(
      id: 3,
      title: "Kurta_3",
      price: 234,
      size: 10,
      description: dummyText,
      images: ["assets/images/man3.png","assets/images/man_3_1.png","assets/images/man_3_2.png"],
      image: "assets/images/man3.png",
      color: Color(0xFF989493)),
  Product(
      id: 4,
      title: "Old Fashion_Kurta",
      price: 234,
      size: 11,
      images: ["assets/images/man4.png","assets/images/man_4_1.png","assets/images/man_4_2.png"],
      description: dummyText,
      image: "assets/images/man4.png",
      color: Color(0xFFE6B398)),
  Product(
      id: 5,
      title: "Office_Kurta",
      price: 234,
      size: 12,
      images: ["assets/images/man5.png","assets/images/man_5_1.png","assets/images/man_5_2.png"],
      description: dummyText,
      image: "assets/images/man5.png",
      color: Color(0xFFFB7883)),
  Product(
    id: 6,
    title: "Kurta_brown",
    price: 234,
    size: 12,
    description: dummyText,
    images: ["assets/images/man6.png","assets/images/man_6_1.png","assets/images/man_6_2.png"],
    image: "assets/images/man6.png",
    color: Color(0xFFAEAEAE),
  ),
  Product(
    id: 7,
    title: "Women Kurta",
    price: 200,
    size: 12,
    description: dummyText,
    images: ["assets/images/women1.png","assets/images/women_1_1.png","assets/images/women_1_2.png"],
    image: "assets/images/women1.png",
    color: Color(0xFF293B3E),
  ),
  Product(
    id: 8,
    title: "Single yellow Kurta",
    price: 234,
    size: 12,
    description: dummyText,
    images: ["assets/images/women2.png","assets/images/women_2_1.png","assets/images/women_2_2.png"],
    image: "assets/images/women2.png",
    color: Color(0xFFE9BA19),
  )
];

String dummyText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    "Lorem Ipsum has been the industry's standard dummy text ever since. When an unknown printer took a galley.";
