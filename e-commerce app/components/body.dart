import 'package:ecommerce_app/app_routes/app_routes.dart';
import 'package:ecommerce_app/constants.dart';
import 'package:ecommerce_app/controllers/shopping_controller.dart';
import 'package:ecommerce_app/models/Product.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'categories.dart';
import 'item_cards.dart';

class Body extends StatelessWidget {
final shoppingController = Get.put(ShoppingController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
          child: Text(
            ("MEN"),
            style: Theme
                .of(context)
                .textTheme
                .headline4!
                .copyWith(fontWeight: FontWeight.bold,color: Colors.black
          
             )

            ,),

          ),

        Categories(),
        Obx((){
          if(Events.LOADING == (shoppingController.events)){
            return Expanded(flex:1, child: Container(child: Center(child: CircularProgressIndicator(color: Colors.red,),),));
          }else if(shoppingController.events == Events.FETCH){
            return Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
                  child: GridView.builder(
                    itemCount: shoppingController.products.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: kDefaultPaddin,
                      crossAxisSpacing: kDefaultPaddin,
                      childAspectRatio: 0.75,
                    ),

                    itemBuilder: (context, index) =>
                        ItemCard(product: shoppingController.products[index],
                            press: () {
                              /*Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      DetailsScreen(product: products[index],)))*/
                              Get.toNamed(AppRoutes.homeDetailsRoute,
                                  arguments: products[index]);

                            }

                        ),

                  ),
                )
            );
          }else{
            return Expanded(flex: 1,child: Container(child: Center(child: Text("Something went wrong..", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),)),));
          }
        })
      ],
    );
  }
}


