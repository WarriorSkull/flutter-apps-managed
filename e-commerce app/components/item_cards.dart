import 'package:ecommerce_app/models/Product.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class ItemCard extends StatelessWidget {
  final Product product;
  final void Function() press;

  const ItemCard({
    //required Key key,
    required this.product,
    required this.press,
  }); //: super(key: );

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            child: Container(
              padding: EdgeInsets.all((1)),
              // height: 180,
              // width: 160,
              decoration: BoxDecoration(
                  //color: product.color,
                  ),
              child: Hero(
                  tag: "${product.id}",
                  child: Image.asset(
                    product.image,
                    fit: BoxFit.cover,
                  )),
            ),
          )),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
              child: Text(
                product.title,
                style: TextStyle(color: kTextLightColor),
              )),
          Text(
            "\$${product.price}",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
